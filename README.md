# Getting Started

## Prerequisites

### Docker
Installation Guide: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04

### Docker Compose
Installation Guide: https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-20-04


# Usage

### Install

    docker-compose build

### Run the app

    docker-compose up -d

### Stop the app

    docker-compose down

### Restart the app

    docker-compose restart