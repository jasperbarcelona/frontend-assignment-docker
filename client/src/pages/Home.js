import { useState, useEffect } from "react";
import { Injected } from "../components/Connectors";
import { useWeb3React } from "@web3-react/core";
import { useSnackbar } from "react-simple-snackbar";
import Web3 from "web3";

import Section from "../components/Section";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Particle from "../components/Particle";
import Modal from "react-bootstrap/Modal";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import Loader from "../components/Loader";
import Skeleton from "@mui/material/Skeleton";

const Home = () => {
	const { active, account, activate, deactivate, chainId, error } =
		useWeb3React();

	const [nep, setNep] = useState(0);
	const [busd, setBusd] = useState(0);
	const [balance, setBalance] = useState("");
	const [showModal, setShowModal] = useState(false);
	const [errorMsg, setErrorMsg] = useState("");
	const [loading, setLoading] = useState(false);
	const [balanceLoading, setBalanceLoading] = useState(false);
	const [connectAttempt, setConnectAttempt] = useState(false);

	const formatter = new Intl.NumberFormat("en-US", {
		minimumFractionDigits: 2,
		maximumFractionDigits: 2,
	});

	const [defaultSnackbar] = useSnackbar({
		style: {
			fontSize: 12,
			fontWeight: "bold",
		},
	});

	const [warningSnackbar] = useSnackbar({
		style: {
			backgroundColor: "orange",
			color: "#fff",
			fontSize: 12,
			fontWeight: "bold",
		},
	});

	const [dangerSnackbar] = useSnackbar({
		style: {
			backgroundColor: "#d81e5b",
			color: "#fff",
			fontSize: 12,
			fontWeight: "bold",
		},
	});

	const [successSnackbar] = useSnackbar({
		style: {
			backgroundColor: "#85c0ed",
			color: "#fff",
			fontSize: 12,
			fontWeight: "bold",
		},
	});

	const convertCurrency = (baseCurrency, value) => {
		if (baseCurrency === "nep") {
			setNep(value);
			setBusd(formatter.format(value * 3));
		} else {
			setBusd(value);
			setNep(formatter.format(value / 3));
		}
	};

	const connectWallet = async () => {
		setLoading(true);
		setConnectAttempt(true);
		await activate(Injected);
		setLoading(false);
	};

	const disconnectWallet = () => {
		deactivate();
		setShowModal(false);
		setErrorMsg("");
		warningSnackbar("Wallet disconnected");
	};

	const getBalance = async (account) => {
		setBalanceLoading(true);
		const provider = new Web3(window.web3.currentProvider);
		await provider.eth
			.getBalance(account)
			.then((res) => {
				setBalance(provider.utils.fromWei(res));
				setErrorMsg("");
				setBalanceLoading(false);
			})
			.catch((err) => {
				console.log(err);
				setBalance("Failed to fetch");
				setErrorMsg(
					"Please check if you are in the correct Metamask network (BSC - Testnet)"
				);
				setBalanceLoading(false);
			});
	};

	const copyAddress = () => {
		navigator.clipboard.writeText(account);
		defaultSnackbar("Copied to clipboard");
	};

	useEffect(() => {
		if (account) {
			getBalance(account);
		}
	}, [account, chainId]);

	useEffect(() => {
		if (connectAttempt) {
			if (error) {
				dangerSnackbar("Could not connect");
				setErrorMsg(
					"Make sure you have Metamask installed and set to the correct network (BSC - Testnet)"
				);
			} else {
				successSnackbar("Wallet connected");
			}
		}
	}, [loading, error]);

	return (
		<>
			<Particle />
			<Section>
				<Container className="full-page flex-column">
					<div
						className={`convert-form bg-white rounded col-lg-4 col-md-8 col-12 ${
							showModal ? "hidden" : ""
						}`}
					>
						<h3 className="convert-title">Crypto Converter</h3>
						<TextField
							type="number"
							label="NEP"
							autoComplete="off"
							className="h-full"
							value={nep && nep !== "0.00" ? nep : ""}
							onChange={(e) => convertCurrency("nep", e.target.value)}
						/>
						<div className="convert-icon-container align-center">
							<i className="ri-exchange-line color-dark text-xl opacity-5" />
						</div>
						<TextField
							type="number"
							label="BUSD"
							autoComplete="off"
							className="h-full"
							value={busd && busd !== "0.00" ? busd : ""}
							onChange={(e) => convertCurrency("busd", e.target.value)}
						/>
						<Button
							variant="text"
							className="check-wallet-btn color-dark h-full"
							onClick={() => setShowModal(true)}
						>
							Check Wallet Details
						</Button>
					</div>
				</Container>
			</Section>
			<Modal
				show={showModal}
				onHide={() => setShowModal(false)}
				className="wallet-modal"
				centered
			>
				<Modal.Header>
					<Modal.Title className="text-lg">Wallet details</Modal.Title>
				</Modal.Header>
				<Modal.Body className="pt-4 pb-4 flex-column">
					{loading ? (
						<Loader />
					) : active ? (
						<Table className="account-table h-full text-sm">
							<tbody>
								<tr>
									<td>Address</td>
									<td className="align-right">
										<span
											className="address-preview flex-row-right pull-right"
											onClick={copyAddress}
										>
											<i className="ri-file-copy-fill" />
											{account.substring(0, 4)}...
											{account.substring(account.length - 4)}
										</span>
									</td>
								</tr>
								<tr>
									<td>Chain ID</td>
									<td className="align-right">{chainId}</td>
								</tr>
								<tr>
									<td>Balance</td>
									<td className="align-right">
										{balanceLoading ? (
											<Skeleton className="text-sm pull-right" width={70} />
										) : (
											balance
										)}
									</td>
								</tr>
							</tbody>
						</Table>
					) : (
						<p className="color-orange text-sm align-center m-0">
							Wallet not connected.
						</p>
					)}
					{errorMsg && (
						<p className="error-msg align-center text-xs italic">{errorMsg}</p>
					)}
				</Modal.Body>
				<Modal.Footer>
					<Button
						variant="text"
						className="modal-btn color-dark"
						onClick={() => setShowModal(false)}
					>
						Close
					</Button>
					<Button
						variant="contained"
						className="modal-btn bg-dark"
						disabled={loading}
						onClick={active ? disconnectWallet : () => connectWallet()}
					>
						{loading ? "Loading.." : active ? "Disconnect" : "Connect"}
					</Button>
				</Modal.Footer>
			</Modal>
		</>
	);
};

export default Home;
